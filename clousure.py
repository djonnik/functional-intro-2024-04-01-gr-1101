from typing import Callable


def power_factory(exp: int) -> Callable:
    def power(base: int) -> int:
        return base ** exp

    return power


if __name__ == '__main__':
    square = power_factory(2)
    cube = power_factory(3)
    print(square(5))
    print(cube(3))